ARG BASE_IMAGE="nvidia/cuda:9.0-base-ubuntu16.04"
FROM $BASE_IMAGE

########################################
# Essentials
########################################

RUN apt-get update && \
DEBIAN_FRONTEND=noninteractive apt-get install -y \
    build-essential \
    curl \
    git \
    lsb-release \
    python-dev \
    software-properties-common \
    unzip \
    zip \
    && \
rm -rf /var/lib/apt/lists/*

RUN curl -fSsL -O https://bootstrap.pypa.io/get-pip.py && \
    python get-pip.py && \
    rm get-pip.py

########################################
# Tensorflow
########################################

# Pick up some TF dependencies
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        build-essential \
        curl \
        git \
        libcudnn7 \
        libcudnn7-dev \
        libcurl3-dev \
        libfreetype6-dev \
        libhdf5-serial-dev \
        libzmq3-dev \
        pkg-config \
        python-dev \
        rsync \
        software-properties-common \
        unzip \
        zip \
        zlib1g-dev \
        wget \
    && \
    if [ -n "$(apt-cache search cuda-command-line-tools-9-0)" ]; then \
        apt-get install -y --no-install-recommends \
            cuda-command-line-tools-9-0 \
            cuda-cublas-dev-9-0 \
            cuda-cudart-dev-9-0 \
            cuda-cufft-dev-9-0 \
            cuda-curand-dev-9-0 \
            cuda-cusolver-dev-9-0 \
            cuda-cusparse-dev-9-0 \
            libpng12-dev \
    ; fi; \
    rm -rf /var/lib/apt/lists/* && \
    find /usr/local/cuda/lib64/ -type f -name 'lib*_static.a' -not -name 'libcudart_static.a' -delete && \
    rm -f /usr/lib/x86_64-linux-gnu/libcudnn_static_v7.a

RUN apt-get update && \
    if [ -n "$(apt-cache search cuda-command-line-tools-9-0)" ]; then \
        apt-get install nvinfer-runtime-trt-repo-ubuntu1604-4.0.1-ga-cuda9.0 && \
        apt-get update && \
        apt-get install libnvinfer4=4.1.2-1+cuda9.0 && \
        apt-get install libnvinfer-dev=4.1.2-1+cuda9.0 ; \
    fi; \
    rm -rf /var/lib/apt/lists/*

RUN pip --no-cache-dir install \
        Pillow \
        h5py \
        ipykernel \
        jupyter \
        keras_applications \
        keras_preprocessing \
        matplotlib \
        mock \
        numpy \
        scipy \
        sklearn \
        pandas \
        && \
    python -m ipykernel.kernelspec

RUN pip --no-cache-dir install tensorflow_gpu

########################################
# Face-recognition
########################################

RUN apt-get update && \
DEBIAN_FRONTEND=noninteractive apt-get install -y \
    build-essential \
    cmake \
    gfortran \
    git \
    wget \
    curl \
    graphicsmagick \
    libgraphicsmagick1-dev \
    libavcodec-dev \
    libavformat-dev \
    libgtk2.0-dev \
    libjpeg-dev \
    liblapack-dev \
    libswscale-dev \
    pkg-config \
    python-dev \
    python-numpy \
    software-properties-common \
    zip \
    && \
if [ -n "$(apt-cache search libatlas-dev)" ]; then \
DEBIAN_FRONTEND=noninteractive apt-get install -y \
    libatlas-dev \
; fi; \
rm -rf /var/lib/apt/lists/*

RUN cd ~ && \
    mkdir -p dlib && \
    git clone -b 'v19.9' --single-branch https://github.com/davisking/dlib.git dlib/ && \
    cd  dlib/ && \
    python setup.py install --yes USE_AVX_INSTRUCTIONS

########################################
# NaoQI
########################################

# Install pynaoqi and fix permissions
RUN cd /usr/local && \
curl -s 'http://blackhole.themagiclab.org/pepper_os/pynaoqi-python2.7-2.5.5.5-linux64.tar.gz' \
| tar xz && \
cd /usr/local/pynaoqi-python2.7-2.5.5.5-linux64 && \
chmod -R +r . && \
find . -type d -exec chmod +x {} \;

########################################
# ROS-perception
########################################

# install packages
RUN apt-get update && \
DEBIAN_FRONTEND=noninteractive apt-get install -q -y \
    dirmngr \
    gnupg2 \
    lsb-release \
&& rm -rf /var/lib/apt/lists/*

# setup keys
RUN apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654

# setup sources.list
RUN echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list

# install bootstrap tools
RUN apt-get update && \
DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y \
    python-rosdep \
    python-rosinstall \
    python-vcstools \
&& rm -rf /var/lib/apt/lists/*

# setup environment
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

# bootstrap rosdep
RUN rosdep init \
    && rosdep update

# install ros packages
ARG ROS_DISTRO=kinetic
ENV ROS_DISTRO=$ROS_DISTRO
RUN apt-get update && \
DEBIAN_FRONTEND=noninteractive apt-get install -y \
    ros-${ROS_DISTRO}-ros-core \
&& rm -rf /var/lib/apt/lists/*
RUN apt-get update && \
DEBIAN_FRONTEND=noninteractive apt-get install -y \
    ros-${ROS_DISTRO}-ros-base \
&& rm -rf /var/lib/apt/lists/*
RUN apt-get update && \
DEBIAN_FRONTEND=noninteractive apt-get install -y \
    ros-${ROS_DISTRO}-perception \
&& rm -rf /var/lib/apt/lists/*


########################################
# ROS-magiclab
########################################

# Install debian packages
# (allow install to fail for packages not available on some distros)
RUN apt-get update && \
DEBIAN_FRONTEND=noninteractive apt-get install -y \
    file \
    iputils-ping \
    libasound2-dev \
    libopus-dev \
    libopus0 \
    libpulse-dev \
    libsndfile1 \
    nano \
    net-tools \
    openssh-client \
    openvpn \
    pulseaudio-utils \
    ros-${ROS_DISTRO}-ddynamic-reconfigure-python \
    ros-${ROS_DISTRO}-move-base-flex \
    ros-${ROS_DISTRO}-rosbridge-suite \
    ros-${ROS_DISTRO}-rqt \
    ros-${ROS_DISTRO}-smach-ros \
    sudo \
    swig \
    traceroute \
    unzip && \
DEBIAN_FRONTEND=noninteractive apt-get install -y \
    ros-${ROS_DISTRO}-naoqi-bridge-msgs || echo ok && \
DEBIAN_FRONTEND=noninteractive apt-get install -y \
    ros-${ROS_DISTRO}-web-video-server || echo ok && \
rm -rf /var/lib/apt/lists/*

# Install pip packages
RUN pip --no-cache-dir install --upgrade setuptools
# distutils-installed packages cannot be upgraded by pip
RUN /usr/local/bin/easy_install --upgrade \
    numpy \
    pyasn1-modules \
    PyOpenSSL \
    tornado
RUN pip --no-cache-dir install --upgrade \
    'ipython[notebook]<=6.0' \
    'requests[security]' \
    'tornado<5.0' \
    configobj \
    dill \
    google-cloud-speech \
    keras \
    pocketsphinx \
    pydash \
    PyDispatcher \
    scikit-image \
    scipy \
    theano \
    Watchdog \
    websocket-client \
    ws4py \
    xxhash

# Add tools for building .deb files
RUN apt-get update && \
DEBIAN_FRONTEND=noninteractive apt-get install -y \
    build-essential \
    devscripts \
    dpatch \
    equivs \
    fakeroot \
    lintian \
    python-bloom \
    python-catkin-tools \
    quilt && \
rm -rf /var/lib/apt/lists/*

# Create nonprivileged user to run rosdep
RUN useradd --create-home --shell=/bin/bash rosuser
RUN sudo -u rosuser -H rosdep update

########################################
# Final setup
########################################

# Add scripts from this package
ADD assets /
RUN chmod -R o-w /etc/sudoers.d
RUN chown -R rosuser:rosuser /home/rosuser/

WORKDIR /home/rosuser/
USER rosuser

# Interactive niceties
ENV TERM xterm-color
ENV EDITOR nano -wi

# Publish roscore port
EXPOSE 11311 9090 8080 8888

# pre-install magic packages
RUN sudo apt-get update -o Dir::Etc::sourcelist="sources.list.d/blackhole.list" \
    -o Dir::Etc::sourceparts="-" -o APT::Get::List-Cleanup="0" && \
DEBIAN_FRONTEND=noninteractive sudo apt-get install -y \
    ros-${ROS_DISTRO}-analytics-tracking \
    ros-${ROS_DISTRO}-command-executer \
    ros-${ROS_DISTRO}-magic-faces \
    ros-${ROS_DISTRO}-magic-hri \
    ros-${ROS_DISTRO}-magic-metrics \
    ros-${ROS_DISTRO}-magic-msgs \
    ros-${ROS_DISTRO}-magic-poses \
    ros-${ROS_DISTRO}-magic-ros \
    ros-${ROS_DISTRO}-magic-ros-apps \
    ros-${ROS_DISTRO}-magic-speak \
    ros-${ROS_DISTRO}-magic-stylesheets-pepper \
    ros-${ROS_DISTRO}-magic-vision \
    ros-${ROS_DISTRO}-pepper-diagnostics \
    ros-${ROS_DISTRO}-pepper-openni \
    || echo ok && \
sudo rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["/usr/local/bin/ros_entrypoint"]
